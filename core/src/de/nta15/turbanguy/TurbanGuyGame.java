package de.nta15.turbanguy;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

public class TurbanGuyGame extends ApplicationAdapter {
    private static final float TURBAN_START_X = 50;
    private static final float TURBAN_START_Y = 240;

    float score;
    int highscore;
    SpriteBatch batch;
    OrthographicCamera camera;
    Texture menu;
    Texture background;
    Texture arrow1;
    Music music;
    Animation Turban;
    Vector2 Turbanpos = new Vector2();
    float time = 0;
    Vector3 touchPos = new Vector3();
    Vector3 w = new Vector3();
    Vector3 breakpos = new Vector3();
    Vector3 arrowpos = new Vector3();
    boolean arrow = false;
    float arrowpositionX;
    float arrowpositionY = 80;
    boolean arrowshowed = false;
    boolean music_ = true;
    boolean sounds_ = true;
    int backgroundY;
    int speed = 0;
    float time2;
    float time3;
    GameState gameState = GameState.Start;

    enum GameState {
        Start, Running, GameOver, Guide1, Guide2, Guide3, Shop1, Shop2, Shop3, Settings, Credits, Break;
    }

    BitmapFont font;

    TextureRegion guide1;
    TextureRegion guide2;
    TextureRegion guide3;
    TextureRegion shop1;
    TextureRegion shop2;
    TextureRegion shop3;
    TextureRegion settings;
    TextureRegion credits;
    TextureRegion pause;
    TextureRegion settings_1_pushed;
    TextureRegion settings_2_pushed;
    TextureRegion settings_both_pushed;
    TextureRegion break_screen;
    TextureRegion hole;
    Animation spider;
    Animation scorpion;
    TextureRegion gameover_screen;

    Array<Vector2> holePositions = new Array<Vector2>();
    Array<Vector2> spiderPositions = new Array<Vector2>();
    Array<Vector2> scorpionPositions = new Array<Vector2>();

    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);

        menu = new Texture("menu_ready.png");
        background = new Texture("background.png");
        arrow1 = new Texture("pfeil1.png");

        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, menu.getWidth(), menu.getHeight());

        music = Gdx.audio.newMusic(Gdx.files.internal("soundtrackloop.mid"));
        music.setLooping(true);
        music.play();

        font = new BitmapFont(Gdx.files.internal("arial.fnt"));

        Texture frame1 = new Texture("TurbanGuycharakter1.png");
        Texture frame2 = new Texture("TurbanGuycharakter2,6.png");
        Texture frame3 = new Texture("TurbanGuycharakter3,5.png");
        Texture frame4 = new Texture("TurbanGuycharakter4.png");
        Texture frame5 = new Texture("TurbanGuycharakter3,5.png");
        Texture frame6 = new Texture("TurbanGuycharakter2,6.png");
        Texture spider1 = new Texture("spider1.png");
        Texture spider2 = new Texture("spider2.png");
        Texture scorpion1 = new Texture("scorpion1.png");
        Texture scorpion2 = new Texture("scorpion2.png");
        Turban = new Animation(0.05f, new TextureRegion(frame1), new TextureRegion(frame2), new TextureRegion(frame3),
                new TextureRegion(frame4), new TextureRegion(frame5), new TextureRegion(frame6));
        Turban.setPlayMode(PlayMode.LOOP);

        guide1 = new TextureRegion(new Texture("guide_page1.png"));
        guide2 = new TextureRegion(new Texture("guide_page2.png"));
        guide3 = new TextureRegion(new Texture("guide_page3.png"));
        shop1 = new TextureRegion(new Texture("shop_page1.png"));
        shop2 = new TextureRegion(new Texture("shop_page2.png"));
        shop3 = new TextureRegion(new Texture("shop_page3.png"));
        settings = new TextureRegion(new Texture("settings.png"));
        credits = new TextureRegion(new Texture("credits.png"));
        pause = new TextureRegion(new Texture("break.png"));
        settings_1_pushed = new TextureRegion(new Texture("settings_music_pushed.png"));
        settings_2_pushed = new TextureRegion(new Texture("settings_sounds_pushed.png"));
        settings_both_pushed = new TextureRegion(new Texture("settings_both_pushed.png"));
        break_screen = new TextureRegion(new Texture("break_screen.png"));
        hole = new TextureRegion(new Texture("Hole.png"));
        spider = new Animation(0.1f, new TextureRegion(spider1), new TextureRegion(spider2));
        spider.setPlayMode(PlayMode.LOOP);
        scorpion = new Animation(0.1f, new TextureRegion(scorpion1), new TextureRegion(scorpion2));
        scorpion.setPlayMode(PlayMode.LOOP);
        gameover_screen = new TextureRegion(new Texture("score.png"));

        Preferences pref = Gdx.app.getPreferences("mydata.dat");
        highscore = pref.getInteger("highscore", 0);

        resetWorld();
    }

    private void resetWorld() {
        Gdx.app.debug("TurbanGuy", camera.position.toString());
        camera.position.x = menu.getWidth() / 2 + 10;
        camera.position.y = menu.getHeight() / 2;
        Turbanpos.set(TURBAN_START_X, TURBAN_START_Y);
        time = 0;
        backgroundY = 0;

        holePositions.clear();
        for (int hole_i = 0; hole_i < 2; hole_i++) {
            int hole_x = MathUtils.random((int) camera.viewportWidth);
            int hole_y = hole_i * 700 + 700;                                    //change
            holePositions.add(new Vector2(hole_x, hole_y));
        }

        spiderPositions.clear();
        for (int spider_i = 0; spider_i < 1; spider_i++) {
            int spider_x = MathUtils.random((int) camera.viewportWidth);
            int spider_y = spider_i * 700 + 700;                                    //change
            spiderPositions.add(new Vector2(spider_x, spider_y));
        }

        scorpionPositions.clear();
        for (int scorpion_i = 0; scorpion_i < 1; scorpion_i++) {
            int scorpion_x = MathUtils.random((int) camera.viewportWidth);
            int scorpion_y = scorpion_i * 700 + 700;                                    //change
            scorpionPositions.add(new Vector2(scorpion_x, scorpion_y));
        }
    }


    private void updateWorld() {
        if (gameState != GameState.Break) {

            float deltaTime = Gdx.graphics.getDeltaTime();
            time += deltaTime;
            if (gameState != GameState.GameOver) {
                score = time * 5;
            }
            if (score > highscore) {
                highscore = (int) score;
            }


            if (gameState == GameState.Running) {
                Turbanpos.x += 0.25f * Gdx.input.getRoll();
                Turbanpos.y = camera.position.y - 350;
                breakpos.y = camera.position.y + 340;
                //arrowpos.y = camera.position.y - 230;
                camera.position.y += 12;

                if (camera.position.y - camera.viewportHeight / 2 > backgroundY + background.getHeight()) {
                    backgroundY += background.getHeight();
                }

                if (Turbanpos.x <= 0) {
                    Turbanpos.x = 0;
                }
                if (Turbanpos.x >= 420) {
                    Turbanpos.x = 419;
                }
            }

            if (Gdx.input.justTouched() && gameState == GameState.Running && time >= 1) {
                arrow = true;
            }

        }

        if (Gdx.input.justTouched()) {

            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            float y = touchPos.y;
            System.out.println("y " + y);
            float x = touchPos.x;
            System.out.println("x " + x);

            if (gameState == GameState.Start) {
                if (y > 512 && y < 621 && gameState == GameState.Start) {
                    arrow = false;
                    arrowshowed = false;
                    time = 0f;
                    time3 = 0f;
                    speed = 0;
                    gameState = GameState.Running;
                    resetWorld();
                }
                if (y > 422 && y < 486 && gameState == GameState.Start) {
                    gameState = GameState.Shop1;
                }
                if (y > 310 && y < 400 && gameState == GameState.Start) {
                    gameState = GameState.Guide1;
                }
                if (y > 170 && y < 262 && gameState == GameState.Start) {
                    gameState = GameState.Settings;
                }
            } else if (gameState == GameState.Guide1) {
                if (y > 57 && y < 120 && x > 336 && x < 442) {
                    gameState = GameState.Guide2;
                } else if (y > 58 && y < 149 && x > 206 && x < 305) {
                    gameState = GameState.Start;
                    System.out.println("START!");
                }
            } else if (gameState == GameState.Guide2) {
                if (y > 57 && y < 120 && x > 336 && x < 442) {
                    gameState = GameState.Guide3;
                    System.out.println("GUIDE3");
                } else if (y > 70 && y < 134 && x > 60 && x < 175) {
                    gameState = GameState.Guide1;
                } else if (y > 58 && y < 149 && x > 206 && x < 305) {
                    gameState = GameState.Start;
                }
            } else if (gameState == GameState.Guide3) {
                if (y > 70 && y < 134 && x > 60 && x < 175) {
                    gameState = GameState.Guide2;
                } else if (y > 58 && y < 149 && x > 206 && x < 305) {
                    gameState = GameState.Start;
                }
            } else if (gameState == GameState.Shop1) {
                if (y > 57 && y < 120 && x > 336 && x < 442) {
                    gameState = GameState.Shop2;
                    System.out.println("SHOP2");
                } else if (y > 58 && y < 149 && x > 206 && x < 305) {
                    gameState = GameState.Start;
                }
            } else if (gameState == GameState.Shop2) {
                if (y > 57 && y < 120 && x > 336 && x < 442) {
                    gameState = GameState.Shop3;
                } else if (y > 70 && y < 134 && x > 60 && x < 175) {
                    gameState = GameState.Shop1;
                } else if (y > 58 && y < 149 && x > 206 && x < 305) {
                    gameState = GameState.Start;
                }
            } else if (gameState == GameState.Shop3) {
                if (y > 70 && y < 134 && x > 60 && x < 175) {
                    gameState = GameState.Shop2;
                } else if (y > 58 && y < 149 && x > 206 && x < 305) {
                    gameState = GameState.Start;
                }
            } else if (gameState == GameState.Settings) {
                if (y > 58 && y < 149 && x > 206 && x < 305) {
                    gameState = GameState.Start;
                } else if (y > 507 && y < 546 && x > 75 && x < 419) {
                    gameState = GameState.Credits;
                } else if (y > 565 && y < 604 && x > 75 && x < 419) { //Sound
                    if (sounds_ == true) {
                        sounds_ = false;
                    } else {
                        sounds_ = true;
                    }
                    //sounds
                } else if (y > 617 && y < 665 && x > 75 && x < 419) { //Musik
                    if (music_ == true) {
                        music_ = false;
                    } else {
                        music_ = true;
                    }
                    music();
                }
            } else if (gameState == GameState.Credits) {
                if (y > 80 && y < 127 && x > 64 && x < 177) {
                    gameState = GameState.Settings;
                }
            } else if (gameState == GameState.GameOver) {
                if (y > Turbanpos.y + 75 && y < Turbanpos.y + 225 && x > 65 && x < 190) {
                    resetWorld();
                    gameState = GameState.Running;
                } else if (y > Turbanpos.y + 75 && y < Turbanpos.y + 225 && x > 300 && x < 440) {
                    resetWorld();
                    gameState = GameState.Start;
                }
            } else if (gameState == GameState.Break) {
                if (y > Turbanpos.y - 20 && y < Turbanpos.y + 80 && x > 206 && x < 305) {
                    arrow = false;
                    gameState = GameState.Start;
                    resetWorld();
                    drawWorld();
                } else if (y > Turbanpos.y + 300 && y < Turbanpos.y + 800 && x > 46 && x < 450) {
                    gameState = GameState.Running;
                    arrow = false;
                }
            }
        }

        if (gameState == GameState.Running) {
            if (gameState == GameState.Break) {
                if (Gdx.input.justTouched()) {
                    touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
                    camera.unproject(touchPos);
                    float y = touchPos.y;
                    float x = touchPos.x;
                    if (x >= 406 && x <= 490 && y >= 700) {
                        gameState = GameState.Running;
                        System.out.println("HOOO");
                    }
                }
            }
            if (Gdx.input.justTouched()) {
                touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
                camera.unproject(touchPos);
                float y = touchPos.y;
                float x = touchPos.x;
                if (x >= 406 && x <= 490 && y >= Turbanpos.y + 600) {
                    gameState = GameState.Break;
                }
            }
            float hole_maxY = hole_maxY(holePositions);

            for (Vector2 holePosition : holePositions) {
                if (holePosition.y < camera.position.y - 480 - hole.getRegionHeight()) {
                    holePosition.x = MathUtils.random(480 - hole.getRegionWidth());
//					holePosition.y += 700 + MathUtils.random(100);
                    holePosition.y = hole_maxY + 400 + MathUtils.random(300);
                }
            }

            for (Vector2 holePosition : holePositions) {
                Rectangle holeRect = new Rectangle(
                        holePosition.x, holePosition.y,
                        hole.getRegionWidth(), hole.getRegionHeight());
                if (holeRect.contains(Turbanpos.x + 40, Turbanpos.y + 10)) {
                    gameState = GameState.GameOver;
                }
            }

            float spider_maxY = spider_maxY(spiderPositions);

            for (Vector2 spiderPosition : spiderPositions) {
                if (spiderPosition.y < camera.position.y - 480 - spider.getKeyFrame(0).getRegionHeight()) {
                    spiderPosition.x = MathUtils.random(480 - spider.getKeyFrame(0).getRegionWidth());
//					spiderPosition.y += 700 + MathUtils.random(100);
                    spiderPosition.y = spider_maxY + 900 + MathUtils.random(300);
                }
            }

            for (Vector2 spiderPosition : spiderPositions) {
                Rectangle spiderRect = new Rectangle(
                        spiderPosition.x, spiderPosition.y,
                        spider.getKeyFrame(0).getRegionWidth(), spider.getKeyFrame(0).getRegionHeight());
                if (spiderRect.contains(Turbanpos.x + 40, Turbanpos.y + 10)) {
                    gameState = GameState.GameOver;
                }
            }

            float scorpion_maxY = scorpion_maxY(scorpionPositions);

            for (Vector2 scorpionPosition : scorpionPositions) {
                if (scorpionPosition.y < camera.position.y - 480 - scorpion.getKeyFrame(0).getRegionHeight()) {
                    scorpionPosition.x = MathUtils.random(480 - scorpion.getKeyFrame(0).getRegionWidth());
//					scorpionPosition.y += 700 + MathUtils.random(100);
                    scorpionPosition.y = scorpion_maxY + 1100 + MathUtils.random(300);
                }
            }

            for (Vector2 scorpionPosition : scorpionPositions) {
                Rectangle scorpionRect = new Rectangle(
                        scorpionPosition.x, scorpionPosition.y,
                        scorpion.getKeyFrame(0).getRegionWidth(), scorpion.getKeyFrame(0).getRegionHeight());
                if (scorpionRect.contains(Turbanpos.x + 40, Turbanpos.y + 10)) {
                    gameState = GameState.GameOver;
                }
            }

        } // end Running
    }

    float hole_maxY(Array<Vector2> positions) {
        float hole_max = -100;
        for (Vector2 position : positions) {
            if (position.y > hole_max) {
                hole_max = position.y;
            }
        }
        return hole_max;
    }

    float spider_maxY(Array<Vector2> positions) {
        float spider_max = -100;
        for (Vector2 position : positions) {
            if (position.y > spider_max) {
                spider_max = position.y;
            }
        }
        return spider_max;
    }

    float scorpion_maxY(Array<Vector2> positions) {
        float scorpion_max = -100;
        for (Vector2 position : positions) {
            if (position.y > scorpion_max) {
                scorpion_max = position.y;
            }
        }
        return scorpion_max;
    }

    private void drawWorld() {

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        if (gameState == GameState.Start) {
            batch.draw(menu, camera.position.x - menu.getWidth() / 2, 0);
        } else if (gameState == GameState.Running) {
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());

            for (Vector2 holePosition : holePositions) {
                batch.draw(hole, holePosition.x, holePosition.y);
            }
            for (Vector2 spiderPosition : scorpionPositions) {
                batch.draw(spider.getKeyFrame(time), spiderPosition.x, spiderPosition.y);
            }
            for (Vector2 scorpionPosition : spiderPositions) {
                batch.draw(scorpion.getKeyFrame(time), scorpionPosition.x, scorpionPosition.y);
            }

            batch.draw(Turban.getKeyFrame(time), Turbanpos.x, Turbanpos.y); //50
            batch.draw(pause, /*Gdx.graphics.getWidth()*/ camera.viewportWidth - 50, breakpos.y);
            font.draw(batch, "" + (int) score, 50, breakpos.y + 20);
            font.draw(batch, "" + highscore, 50, breakpos.y - 20);
        }

        if (gameState != GameState.Break) {
            if (arrow == true && gameState == GameState.Running) {
                if (arrowshowed == false) {
                    arrowpositionX = Turbanpos.x + 30;
                    arrowshowed = true;
                    speed = 0;
                    time3 = time + 0.6f;
                }
                if (time <= time3) {
                    speed += 20;
                    batch.draw(arrow1, arrowpositionX, Turbanpos.y + speed);
                } else {
                    arrow = false;
                    arrowshowed = false;
                }
            }
        } else {
            batch.draw(arrow1, arrowpositionX, Turbanpos.y + speed);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
            batch.draw(Turban.getKeyFrame(time), Turbanpos.x, Turbanpos.y);
            batch.draw(break_screen, 10, Turbanpos.y - 40);
            font.draw(batch, "Score: " + (int) score, 160, breakpos.y - 200);
        }
        // batch.draw(background, 0, 0);
        w.set(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0);
        camera.unproject(w);

        if (gameState == GameState.Guide1) {
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
            batch.draw(guide1, 10, 10);
        }
        if (gameState == GameState.Guide2) {
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
            batch.draw(guide2, 10, 10);
        }
        if (gameState == GameState.Guide3) {
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
            batch.draw(guide3, 10, 10);
        }
        if (gameState == GameState.Shop1) {
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
            batch.draw(shop1, 10, 10);
        }
        if (gameState == GameState.Shop2) {
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
            batch.draw(shop2, 10, 10);
        }
        if (gameState == GameState.Shop3) {
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
            batch.draw(shop3, 10, 10);
        }
        if (gameState == GameState.Settings) {
            if (music_ == true && sounds_ == true) {
                batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
                batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
                batch.draw(settings, 10, 10);
            } else if (music_ == true && sounds_ == false) {
                batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
                batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
                batch.draw(settings_2_pushed, 10, 10);
            } else if (music_ == false && sounds_ == true) {
                batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
                batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
                batch.draw(settings_1_pushed, 10, 10);
            } else if (music_ == false && sounds_ == false) {
                batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
                batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
                batch.draw(settings_both_pushed, 10, 10);
            }
        }
        if (gameState == GameState.Credits) {
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
            batch.draw(credits, 10, 10);
        }
        if (gameState == GameState.GameOver) {
            Preferences pref = Gdx.app.getPreferences("mydata.dat");
            pref.putInteger("highscore", highscore);
            pref.flush();
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY);
            batch.draw(background, camera.position.x - menu.getWidth() / 2, backgroundY + background.getHeight());
            batch.draw(gameover_screen, 10, Turbanpos.y - 40);
            font.draw(batch, "" + (int) score, 280, Turbanpos.y + 550);
            font.draw(batch, "" + highscore, 280, Turbanpos.y + 460);
        }
        batch.end();
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        updateWorld();
        drawWorld();
    }

    public void music() {
        if (music_ == true) {
            music.play();
            System.out.println("musik start");
        } else {
            music.pause();
        }
    }
}